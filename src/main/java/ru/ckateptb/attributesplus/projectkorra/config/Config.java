package ru.ckateptb.attributesplus.projectkorra.config;

import ru.ckateptb.attributesplus.attributesplus.AttributesPlus;
import ru.ckateptb.tableapi.configs.Configurable;

import java.io.File;

@Configurable.ConfigFile()
public class Config extends Configurable {
    @Configurable.ConfigField(name = "attributes.cooldown.display")
    public static String cooldown = "§b§lCooldown: ";

    @Configurable.ConfigField(name = "attributes.stun.display")
    public static String stun = "§d§lStun: ";

    @Configurable.ConfigField(name = "attributes.stun.duration", comment = "in millisecond")
    public static Long stunDuration = 2000L;

    @Configurable.ConfigField(name = "attributes.stun.message")
    public static String stunMessage = "§d§lStunned";

    public Config() {
        super(AttributesPlus.getInstance().getAttributesFolder() + File.separator + "projectkorra");
    }
}