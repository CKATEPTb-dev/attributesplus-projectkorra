package ru.ckateptb.attributesplus.projectkorra;

import ru.ckateptb.attributesplus.attributesplus.objects.Attribute;
import ru.ckateptb.attributesplus.projectkorra.config.Config;
import ru.ckateptb.tableapi.configs.Configurable;

public abstract class PKAttribute extends Attribute {

    @Override
    public String getRequiredPlugin() {
        return "ProjectKorra";
    }

    @Override
    public Configurable getConfig() {
        return new Config(); //todo
    }
}
