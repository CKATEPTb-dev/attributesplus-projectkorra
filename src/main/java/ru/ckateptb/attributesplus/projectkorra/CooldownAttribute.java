package ru.ckateptb.attributesplus.projectkorra;

import com.projectkorra.projectkorra.event.PlayerCooldownChangeEvent;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import ru.ckateptb.attributesplus.attributesplus.objects.Attributed;
import ru.ckateptb.attributesplus.projectkorra.config.Config;

public class CooldownAttribute extends PKAttribute implements Listener {

    @EventHandler
    public void on(PlayerCooldownChangeEvent e) {
        LivingEntity entity = e.getPlayer();
        Attributed aEntity = new Attributed(entity);
        long cooldown = e.getCooldown();
        double cooldownMultiplier = aEntity.get(getDisplay()) / 100;
        e.setCooldown((long) (cooldown - (cooldown * cooldownMultiplier)));
    }


    @Override
    public Listener getListener() {
        return this;
    }

    @Override
    public String getName() {
        return "cooldown";
    }

    @Override
    public String getDisplay() {
        return Config.cooldown;
    }

    @Override
    public String getDescription() {
        return "Ability cooldown reduction";
    }

    @Override
    public Boolean isVariable() {
        return true;
    }
}
