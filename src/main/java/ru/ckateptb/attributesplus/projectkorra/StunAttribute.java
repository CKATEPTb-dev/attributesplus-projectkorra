package ru.ckateptb.attributesplus.projectkorra;

import com.projectkorra.projectkorra.ability.CoreAbility;
import com.projectkorra.projectkorra.airbending.Suffocate;
import com.projectkorra.projectkorra.chiblocking.Paralyze;
import com.projectkorra.projectkorra.util.MovementHandler;
import org.bukkit.Sound;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.projectiles.ProjectileSource;
import ru.ckateptb.attributesplus.attributesplus.objects.Attributed;
import ru.ckateptb.attributesplus.projectkorra.config.Config;

public final class StunAttribute extends PKAttribute implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void highestOn(EntityDamageByEntityEvent e) {
        Entity entityDamager = e.getDamager();
        LivingEntity damager = null;
        if (!e.getCause().equals(EntityDamageByEntityEvent.DamageCause.PROJECTILE))
            if (entityDamager instanceof LivingEntity)
                damager = (LivingEntity) entityDamager;
            else return;
        else if (entityDamager instanceof Arrow) {
            Arrow projectile = (Arrow) entityDamager;
            ProjectileSource shooter = projectile.getShooter();
            if (shooter instanceof LivingEntity)
                damager = (LivingEntity) shooter;
            else return;
        }
        if (damager == null) return;

        LivingEntity entity = (LivingEntity) e.getEntity();
        Attributed aEntity = new Attributed(damager);
        if (Math.random() * 100 <= aEntity.get(getDisplay())) {
            if (entity instanceof Creature) {
                ((Creature) entity).setTarget(null);
            }
            if (entity instanceof Player && Suffocate.isChannelingSphere((Player) entity)) {
                Suffocate.remove((Player) entity);
            }
            MovementHandler mh = new MovementHandler(entity, CoreAbility.getAbility(Paralyze.class));
            mh.stopWithDuration(Config.stunDuration / 1000L * 20L, Config.stunMessage);
            entity.getWorld().playSound(entity.getLocation(), Sound.ENTITY_ENDERDRAGON_HURT, 2.0F, 0.0F);
            damager.getWorld().playSound(entity.getLocation(), Sound.ENTITY_ENDERDRAGON_HURT, 2.0F, 0.0F);
        }
    }

    @Override
    public Listener getListener() {
        return this;
    }

    @Override
    public String getName() {
        return "stun";
    }

    @Override
    public String getDisplay() {
        return Config.stun;
    }

    @Override
    public String getDescription() {
        return "Chance to stun your opponent";
    }

    @Override
    public Boolean isVariable() {
        return true;
    }
}
